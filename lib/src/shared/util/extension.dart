import 'package:flutter/material.dart';

extension BuildContextExt on BuildContext {
  T argument<T>() {
    return ModalRoute.of(this)?.settings.arguments as T;
  }

  ///WITH GETTER
// Object? get argument => ModalRoute.of(this)?.settings.arguments;

  /// get screen height
  double get h => MediaQuery.of(this).size.height;

  /// get screen width
  double get w => MediaQuery.of(this).size.width;

  /// Navigator pop
  void pop() {
    Navigator.of(this).pop();
  }
}
