import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class HomeEventIncrement extends HomeEvent {}

class HomeEventDecrement extends HomeEvent {}

class HomeEventNotificationClicked extends HomeEvent {
  final bool isLoading;

  HomeEventNotificationClicked({required this.isLoading});

  @override
  List<Object?> get props => [isLoading];
}
