import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/src/di/injection_container.dart';
import 'package:testapp/src/features/movie/argument/movie_detail_argument.dart';
import 'package:testapp/src/features/movie/bloc/movieList/movie_list_bloc.dart';
import 'package:testapp/src/features/movie/bloc/movieList/movie_list_event.dart';
import 'package:testapp/src/features/movie/bloc/movieList/movie_list_state.dart';
import 'package:testapp/src/features/movie/ui/movie_detail_screen.dart';
import 'package:testapp/src/shared/widget/card_movie_list.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MovieListScreen extends StatefulWidget {
  static const String routeName = '/movie-list';

  const MovieListScreen({Key? key}) : super(key: key);

  @override
  _MovieListScreenState createState() => _MovieListScreenState();
}

class _MovieListScreenState extends State<MovieListScreen> {
  // final MovieListBloc bloc = MovieListBloc();
  final MovieListBloc bloc = inject<MovieListBloc>();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    bloc.add(MovieListEventGetList());
  }

  void _onLoading() async {
    bloc.add(MovieListEventGetList());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Movie List Screen'),
      ),
      body: SmartRefresher(
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        enablePullDown: true,
        enablePullUp: true,
        child: BlocConsumer<MovieListBloc, MovieListState>(
          bloc: bloc,
          listener: (context, state) {
            _refreshController.loadComplete();
            _refreshController.refreshCompleted();
          },
          builder: (context, state) {
            switch (state.state) {
              case MovieListStates.onLoading:
                return Center(
                  child: CircularProgressIndicator(),
                );
              case MovieListStates.onLoaded:
                final movieList = state.movieList;
                return ListView.separated(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(16),
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 16);
                  },
                  itemCount: movieList.length,
                  itemBuilder: (context, index) {
                    final item = movieList[index];
                    return CardMovieList(
                      title: item.title,
                      cover: item.posterPath,
                      releaseDate: item.releaseDate,
                      voteAverage: item.voteAverage,
                      onTap: () {
                        Navigator.pushNamed(
                            context, MovieDetailScreen.routeName,
                            arguments: MovieDetailArgument(movie: item));
                      },
                    );
                  },
                );
              case MovieListStates.onError:
                return Text(state.errorMessage);
              default:
                return Container();
            }
          },
        ),
      ),
    );
  }
}
