import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/src/di/injection_container.dart';
import 'package:testapp/src/features/movie/bloc/movieList/movie_list_event.dart';
import 'package:testapp/src/features/movie/bloc/movieList/movie_list_state.dart';
import 'package:testapp/src/repository/interface/movie_repository.dart';

class MovieListBloc extends Bloc<MovieListEvent, MovieListState> {
  final MovieRepository _repository = inject();
  int page = 1;

  MovieListBloc() : super(MovieListState()) {
    on<MovieListEventGetList>(_getList);

    add(MovieListEventGetList());
  }

  _getList(MovieListEventGetList event, Emitter<MovieListState> emit) async {
    try {
      if (state.movieList.isEmpty)
        emit(state.copyWith(state: MovieListStates.onLoading));

      final response = await _repository.movieGetList(page: page++);
      final movieList = response.results ?? [];

      emit(
        state.copyWith(
          state: MovieListStates.onLoaded,
          movieList: List.of(state.movieList)..addAll(movieList),
        ),
      );
    } catch (e) {
      print(e.toString());
      emit(state.copyWith(state: MovieListStates.onError));
    }
  }
}
