import 'package:testapp/src/network/model/movie/movie_list_response_model.dart';

class MovieDetailArgument {
  MovieListModel movie;

  MovieDetailArgument({required this.movie});
}
