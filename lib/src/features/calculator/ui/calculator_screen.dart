// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:testapp/src/features/calculator/bloc/calculator_bloc.dart';
// import 'package:testapp/src/features/calculator/bloc/calculator_event.dart';
// import 'package:testapp/src/features/calculator/bloc/calculator_state.dart';

// class CalculatorScreen extends StatefulWidget {
//   static const String routeName = '/calculator';

//   const CalculatorScreen({Key? key}) : super(key: key);

//   @override
//   _CalculatorScreenState createState() => _CalculatorScreenState();
// }

// class _CalculatorScreenState extends State<CalculatorScreen> {
//   late final CalculatorBloc bloc;

//   // int number = 0;
//   bool checkbox = false;
//   bool bookmark = false;

//   // final _number = BehaviorSubject<int>.seeded(50);
//   //
//   // Stream<int> get number => _number.stream;
//   //
//   // Function(int) get addNumber => _number.add;
//   //
//   // int get numberValue => _number.valueOrNull ?? 0;

//   @override
//   void initState() {
//     super.initState();
//     // bloc = context.read<CalculatorBloc>();
//     bloc = CalculatorBloc();
//   }

//   @override
//   void dispose() {
//     bloc.close();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return BlocListener<CalculatorBloc, CalculatorState>(
//       listenWhen: (prev, curr) => prev.isLoading != curr.isLoading,
//       listener: (context, state) {
//         if (state.isLoading) {
//           print('ISLOADING TRUE');
//         }
//       },
//       child: Scaffold(
//         // appBar: AppBar(
//         //   title: Text('Calculator Screen'),
//         //   actions: [
//         //     IconButton(
//         //       onPressed: () {
//         //         setState(() {
//         //           bookmark = !bookmark;
//         //         });
//         //       },
//         //       icon: Icon(bookmark ? Icons.bookmark : Icons.bookmark_outline),
//         //     ),
//         //     IconButton(
//         //       onPressed: () {
//         //         bloc.add(CalculatorEventNotificationClicked(isLoading: false));
//         //       },
//         //       icon: Icon(Icons.notifications),
//         //     ),
//         //     IconButton(
//         //       onPressed: () {
//         //         //function
//         //       },
//         //       icon: Icon(Icons.email_outlined),
//         //     ),
//         //     SizedBox(width: 10),
//         //   ],
//         // ),
//         backgroundColor: Colors.pinkAccent,
//         body: Column(
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           children: [
//             BlocBuilder<CalculatorBloc, CalculatorState>(
//               bloc: bloc,
//               builder: (context, state) {
//                 return Padding(
//                     padding: const EdgeInsets.only(
//                       left: 0,
//                       top: 250,
//                       right: 30,
//                       bottom: 10,
//                     ),
//                     child: Text(
//                       '${state.number}',
//                       style: TextStyle(fontSize: 70, color: Colors.white),
//                       textAlign: TextAlign.end,
//                     ));
//               },
//               buildWhen: (prev, curr) => prev.number != curr.number,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventReset());
//                     },
//                     child: Text(
//                       'AC',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventPlusMinus());
//                     },
//                     child: Text(
//                       '+/-',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventPercent());
//                     },
//                     child: Text(
//                       '%',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventDistribution());
//                     },
//                     child: Text(
//                       ':',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//             SizedBox(
//               height: 15,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventSeven());
//                     },
//                     child: Text(
//                       '7',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventEight());
//                     },
//                     child: Text(
//                       '8',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventNine());
//                     },
//                     child: Text(
//                       '9',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventMultiplication());
//                     },
//                     child: Text(
//                       'x',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//             SizedBox(
//               height: 15,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventFourth());
//                     },
//                     child: Text(
//                       '4',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventFifth());
//                     },
//                     child: Text(
//                       '5',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventSixth());
//                     },
//                     child: Text(
//                       '6',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventDecrement());
//                     },
//                     child: Text(
//                       '-',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//             SizedBox(
//               height: 15,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventOne());
//                     },
//                     child: Text(
//                       '1',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventTwo());
//                     },
//                     child: Text(
//                       '2',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventThree());
//                     },
//                     child: Text(
//                       '3',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventDecrement());
//                     },
//                     child: Text(
//                       '+',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//             SizedBox(
//               height: 15,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 SizedBox(
//                   width: 160,
//                   height: 70,
//                   child: ElevatedButton(
//                       onPressed: () {
//                         print('- button clicked');
//                         bloc.add(CalculatorEventZero());
//                       },
//                       child: Text(
//                         '0',
//                         style: TextStyle(fontSize: 24),
//                       ),
//                       style: ButtonStyle(
//                         shape:
//                             MaterialStateProperty.all<RoundedRectangleBorder>(
//                                 RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(20.0),
//                           side: BorderSide(color: Colors.red),
//                         )),
//                         backgroundColor:
//                             MaterialStateProperty.all<Color>(Colors.lightBlue),
//                       )),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventComma());
//                     },
//                     child: Text(
//                       ',',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 75,
//                   height: 75,
//                   child: ElevatedButton(
//                     onPressed: () {
//                       print('- button clicked');
//                       bloc.add(CalculatorEventEqualto());
//                     },
//                     child: Text(
//                       '=',
//                       style: TextStyle(fontSize: 24),
//                     ),
//                     style: ElevatedButton.styleFrom(
//                       primary: Colors.lightBlue,
//                       shape: CircleBorder(),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         ),

//         // body: Container(
//         //   padding: EdgeInsets.all(20),
//         //   decoration: BoxDecoration(
//         //     color: Colors.blue,
//         //     borderRadius: BorderRadius.circular(20),
//         //   ),
//         //   child: Text(
//         //     'Calculator Screen',
//         //     style: TextStyle(
//         //       fontWeight: FontWeight.bold,
//         //       fontSize: 30.5,
//         //       color: Colors.white,
//         //     ),
//         //   ),
//         // ),
//       ),
//     );
//   }
// }
