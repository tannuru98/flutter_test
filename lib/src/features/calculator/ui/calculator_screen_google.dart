import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/src/shared/widget/button_calculator.dart';
import 'package:testapp/src/features/calculator/bloc/calculator_bloc.dart';
import 'package:testapp/src/features/calculator/bloc/calculator_event.dart';
import 'package:testapp/src/features/calculator/bloc/calculator_state.dart';

class CalculatorScreenGoogle extends StatefulWidget {
  static const String routeName = '/calculator';

  const CalculatorScreenGoogle({Key? key}) : super(key: key);

  @override
  _CalculatorScreenState createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreenGoogle> {
  final CalculatorBloc bloc = CalculatorBloc();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

// Array of button
  final List<String> buttons = [
    'C',
    '+/-',
    '%',
    'DEL',
    '7',
    '8',
    '9',
    '/',
    '4',
    '5',
    '6',
    'x',
    '1',
    '2',
    '3',
    '-',
    '0',
    '.',
    '=',
    '+',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white38,
      body: Column(
        children: <Widget>[
          Column(children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 40, bottom: 10, right: 10),
              alignment: Alignment.centerRight,
              child: BlocBuilder<CalculatorBloc, CalculatorState>(
                bloc: bloc,
                builder: (context, state) {
                  return Text(
                    '${state.userInput}',
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  );
                },
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              alignment: Alignment.centerRight,
              child: BlocBuilder<CalculatorBloc, CalculatorState>(
                bloc: bloc,
                builder: (context, state) {
                  return Text(
                    '${state.answer}',
                    style: TextStyle(
                        fontSize: 40,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  );
                },
              ),
            )
          ]),
          Expanded(
            child: Container(
              child: GridView.builder(
                  itemCount: buttons.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4),
                  itemBuilder: (BuildContext context, int index) {
                    // Clear Button
                    if (index == 0) {
                      return MyButton(
                        buttontapped: () {
                          bloc.add(CalculatorEventReset());
                        },
                        buttonText: buttons[index],
                        color: Colors.blue[50],
                        textColor: Colors.black,
                      );
                    }

                    // +/- button
                    else if (index == 1) {
                      return MyButton(
                        buttonText: buttons[index],
                        color: Colors.blue[50],
                        textColor: Colors.black,
                      );
                    }
                    // % Button
                    else if (index == 2) {
                      return MyButton(
                        buttontapped: () {
                          bloc.add(CalculatorEventPercent());
                        },
                        buttonText: buttons[index],
                        color: Colors.blue[50],
                        textColor: Colors.black,
                      );
                    }
                    // Delete Button
                    else if (index == 3) {
                      return MyButton(
                        buttontapped: () {
                          bloc.add(CalculatorEventDelete());
                        },
                        buttonText: buttons[index],
                        color: Colors.blue[50],
                        textColor: Colors.black,
                      );
                    }
                    // Equal_to Button
                    else if (index == 18) {
                      return MyButton(
                        buttontapped: () {
                          bloc.add(CalculatorEventEqualPressed());
                        },
                        buttonText: buttons[index],
                        color: Colors.orange[700],
                        textColor: Colors.white,
                      );
                    }

                    // other buttons
                    else {
                      return MyButton(
                        buttontapped: () {
                          bloc.add(
                              CalculatorEventOther(userInputs: buttons[index]));
                        },
                        buttonText: buttons[index],
                        color: isOperator(buttons[index])
                            ? Colors.blueAccent
                            : Colors.white,
                        textColor: isOperator(buttons[index])
                            ? Colors.white
                            : Colors.black,
                      );
                    }
                  }), // GridView.builder
            ),
          ),
        ],
      ),
    );
  }

  bool isOperator(String x) {
    if (x == '/' || x == 'x' || x == '-' || x == '+' || x == '=') {
      return true;
    }
    return false;
  }
}
