import 'package:equatable/equatable.dart';

class CalculatorState extends Equatable {
  String userInput;
  String answer;

  CalculatorState({
    this.userInput = '',
    this.answer = '0',
  });

  CalculatorState copyWith({
    String? userInput,
    String? answer,
  }) {
    return CalculatorState(
      userInput: userInput ?? this.userInput,
      answer: answer ?? this.answer,
    );
  }

  @override
  List<Object?> get props => [userInput, answer];
}
