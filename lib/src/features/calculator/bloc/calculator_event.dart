import 'package:equatable/equatable.dart';

abstract class CalculatorEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class CalculatorEventReset extends CalculatorEvent {}

class CalculatorEventPlusMinus extends CalculatorEvent {}

class CalculatorEventPercent extends CalculatorEvent {}

class CalculatorEventDelete extends CalculatorEvent {}

class CalculatorEventOther extends CalculatorEvent {
  final String userInputs;

  CalculatorEventOther({required this.userInputs});

  @override
  List<Object?> get props => [userInputs];
}

class CalculatorEventEqualPressed extends CalculatorEvent {}
