import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/src/features/calculator/bloc/calculator_event.dart';
import 'package:testapp/src/features/calculator/bloc/calculator_state.dart';
import 'package:math_expressions/math_expressions.dart';

class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  CalculatorBloc() : super(CalculatorState()) {
    on<CalculatorEventReset>(_reset);
    // on<CalculatorEventPlusMinus>(_plusminus);
    on<CalculatorEventPercent>(_percent);
    on<CalculatorEventDelete>(_delete);
    on<CalculatorEventOther>(_other);
    on<CalculatorEventEqualPressed>(_equalPressed);
  }

  _reset(CalculatorEventReset event, Emitter<CalculatorState> emit) {
    emit(state.copyWith(userInput: '', answer: '0'));
  }

  // _plusminus(CalculatorEventPlusMinus event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: number + 1));
  // }

  _percent(CalculatorEventPercent event, Emitter<CalculatorState> emit) {
    state.userInput = state.userInput * 100;
  }

  _delete(CalculatorEventDelete event, Emitter<CalculatorState> emit) {
    state.userInput = state.userInput.substring(0, state.userInput.length - 1);
  }

  _other(CalculatorEventOther event, Emitter<CalculatorState> emit) {
    emit(state.copyWith(userInput: state.userInput += event.userInputs));
  }

  _equalPressed(
      CalculatorEventEqualPressed event, Emitter<CalculatorState> emit) {
    String finaluserinput = state.userInput;
    finaluserinput = state.userInput.replaceAll('x', '*');

    Parser p = Parser();
    Expression exp = p.parse(finaluserinput);
    ContextModel cm = ContextModel();
    double eval = exp.evaluate(EvaluationType.REAL, cm);
    state.answer = eval.toStringAsFixed(0);

    emit(state.copyWith(answer: state.answer));
  }
}

  // _distribution(
  //     CalculatorEventDistribution event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: number + 1));
  // }

  // _seven(CalculatorEventSeven event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 7));
  // }

  // _eight(CalculatorEventEight event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 8));
  // }

  // _nine(CalculatorEventNine event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 9));
  // }

  // _multiplication(
  //     CalculatorEventMultiplication event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: number + 1));
  // }

  // _fourth(CalculatorEventFourth event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 4));
  // }

  // _fifth(CalculatorEventFifth event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 5));
  // }

  // _sixth(CalculatorEventSixth event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 6));
  // }

  // _decrement(CalculatorEventDecrement event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: number - 1));
  // }

  // _one(CalculatorEventOne event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 1));
  // }

  // _two(CalculatorEventTwo event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 2));
  // }

  // _three(CalculatorEventThree event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: 3));
  // }

  // _increment(CalculatorEventIncrement event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: number + 1));
  // }

  // _zero(CalculatorEventZero event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: number + 1));
  // }

  // _comma(CalculatorEventComma event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: number + 1));
  // }

  // _equalto(CalculatorEventEqualto event, Emitter<CalculatorState> emit) {
  //   final number = state.number;
  //   emit(state.copyWith(number: number + 1));
  // }

  // _notificationClicked(CalculatorEventNotificationClicked event,
  //     Emitter<CalculatorState> emit) async {
  //   await Future.delayed(Duration(seconds: 2));
  //   emit(state.copyWith(isLoading: event.isLoading));
  // }
// }
