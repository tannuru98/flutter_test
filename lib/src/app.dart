import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:testapp/src/features/home/bloc/home_bloc.dart';
import 'package:testapp/src/features/movie/ui/movie_screen.dart';
import 'features/home/ui/home_screen.dart';

import 'package:testapp/src/features/calculator/bloc/calculator_bloc.dart';
// import 'features/calculator/ui/calculator_screen.dart';
import 'features/calculator/ui/calculator_screen_google.dart';

import 'features/webview/ui/webview_screen.dart';

import 'features/movie/ui/movie_screen.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(
          create: (context) => HomeBloc(),
        ),
        BlocProvider<CalculatorBloc>(
          create: (context) => CalculatorBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Learning',
        initialRoute: HomeScreen.routeName,
        routes: {
          HomeScreen.routeName: (context) => const HomeScreen(),
          MovieListScreen.routeName: (context) => const MovieListScreen(),
          CalculatorScreenGoogle.routeName: (context) =>
              const CalculatorScreenGoogle(),
          WebViewScreen.routeName: (context) => const WebViewScreen(),
        },
      ),
    );
  }
}
