import 'package:flutter/material.dart';
import 'package:testapp/src/app.dart';
import 'package:testapp/src/di/injection_container.dart' as di;
import 'package:testapp/src/network/api/api_constant.dart';
import 'package:permission_handler/permission_handler.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.location.request();
  await Permission.camera.request();
  await Permission.microphone.request();
  await Permission.storage.request();
  await di.initInjection(ApiConstant.baseUrl);
  runApp(const App());
}
